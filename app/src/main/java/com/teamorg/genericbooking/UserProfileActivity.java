package com.teamorg.genericbooking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class UserProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        Intent intentFromLoginOrRegistration = getIntent();

        String userName = intentFromLoginOrRegistration.getStringExtra("loginUserName");

        TextView userNameTextView = findViewById(R.id.user_name_text);
        userNameTextView.setText(userName);
    }
}
