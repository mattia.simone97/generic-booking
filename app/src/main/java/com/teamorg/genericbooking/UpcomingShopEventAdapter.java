package com.teamorg.genericbooking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.teamorg.genericbooking.model.ShopEvent;

import java.util.List;

public class UpcomingShopEventAdapter extends RecyclerView.Adapter<UpcomingShopEventAdapter.ShopEventViewHolder> {

    private List<ShopEvent> list;
    private Context context;

    public UpcomingShopEventAdapter(List<ShopEvent> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ShopEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.upcoming_event_list, parent, false);

        return new ShopEventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShopEventViewHolder holder, int position) {
        ShopEvent upcomingEvent = list.get(position);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ShopEventViewHolder extends RecyclerView.ViewHolder {


        public ShopEventViewHolder(View itemView) {
            super(itemView);

        }
    }

}
