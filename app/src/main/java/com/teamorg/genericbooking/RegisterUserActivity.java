package com.teamorg.genericbooking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class RegisterUserActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_user);
    }

    public void registerUser(View v) {
        firebaseAuth = FirebaseAuth.getInstance();
        EditText emailEditText = findViewById(R.id.registraion_user_email_input);
        String email = emailEditText.getText().toString();

        EditText passwordEditText = findViewById(R.id.registraion_user_password_input);
        String password = passwordEditText.getText().toString();

        EditText fullNameEditText = findViewById(R.id.registraion_user_name_input);
        String fullName = fullNameEditText.getText().toString();

        final UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(fullName)
                .build();

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        final FirebaseUser user = firebaseAuth.getCurrentUser();

                        user.updateProfile(profileUpdates)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        setupLoggedInUserIntent(user);
                                    }
                                });

                    }
                });

    }

    //TODO: Move in Commons
    private void setupLoggedInUserIntent(FirebaseUser user) {
        if (user != null) {
            String name = user.getDisplayName();

            Intent userProfileIntent = new Intent(this, UserProfileActivity.class);

            userProfileIntent.putExtra("loginUserName", name);

            startActivity(userProfileIntent);
        }
    }
}
