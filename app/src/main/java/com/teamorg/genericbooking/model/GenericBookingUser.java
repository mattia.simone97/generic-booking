package com.teamorg.genericbooking.model;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.teamorg.genericbooking.DatabaseCallback;

import java.util.List;
import java.util.Map;

public class GenericBookingUser {

    FirebaseFirestore db;

    String fullName;
    String email;
    String phoneNumber;

    Map<String, Object> userMap;

    public GenericBookingUser() {
        this.db = FirebaseFirestore.getInstance();
    }


    public void setUserDetails(String fullName, String email, String phoneNumber) {
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;

        userMap.put("fullName", fullName);
        userMap.put("email", email);
        userMap.put("phoneNumber", phoneNumber);
    }

    public void writeNewUser(final DatabaseCallback callback) {
        db.collection("users")
                .add(userMap)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        callback.callback(task.getResult());
                    }
                });
    }
}
