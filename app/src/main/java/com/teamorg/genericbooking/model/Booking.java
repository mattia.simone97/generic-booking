package com.teamorg.genericbooking.model;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teamorg.genericbooking.DatabaseCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Booking {

    FirebaseFirestore db = FirebaseFirestore.getInstance();

    String eventId;
    String userId;
    String shopId;
    boolean paid;

    Map<String, Object> bookingMap;

    public Booking() {
    }


    public static void getBookingsForUserId(String userId, final DatabaseCallback callback) {

        FirebaseFirestore.getInstance()
                .collection("bookings")
                .whereEqualTo("userId", userId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        Map<String, Map<String, Object>> bookingsList = new HashMap<>();
                        for (QueryDocumentSnapshot result : task.getResult()) {
                            bookingsList.put(result.getId(), result.getData());
                        }
                        callback.callback(bookingsList);
                    }
                });

    }

    public static void getBookingbyId(String bookingId, final DatabaseCallback callback) {
        FirebaseFirestore.getInstance()
                .collection("bookings")
                .document(bookingId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        callback.callback(task.getResult().getData());
                    }
                });
    }
}
