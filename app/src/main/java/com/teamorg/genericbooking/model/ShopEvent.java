package com.teamorg.genericbooking.model;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.DayOfWeek;
import com.google.android.libraries.places.api.model.Period;
import com.google.android.libraries.places.api.model.TimeOfWeek;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.teamorg.genericbooking.DatabaseCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShopEvent {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private String eventName;
    private long eventDuration;
    private int eventPriceInCents;
    private int eventCapacity;
    private ArrayList<Period> eventWeekCalendar;
    private ArrayList<EventSlot> bookedEventCalendar;
    private ArrayList<EventSlot> pastBookedEventCalendar;

    public ShopEvent() {
    }

    public ShopEvent(String eventName, long eventDuration, int eventPriceInCents, int eventCapacity) {
        this.eventName = eventName;
        this.eventDuration = eventDuration;
        this.eventPriceInCents = eventPriceInCents;
        this.eventCapacity = eventCapacity;
    }

    public static void getEventById(String eventId, final DatabaseCallback callback) {
        FirebaseFirestore.getInstance()
                .collection("events")
                .document(eventId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        callback.callback(task.getResult().getData());
                    }
                });
    }

    public void setEventWeekCalendar(ArrayList<Period> eventWeekCalendar) {
        this.eventWeekCalendar = eventWeekCalendar;
    }

    public void scheduleBooking(long startTime) {
        EventSlot selectedSlot = getSlotForNewEvent(startTime);

        if (selectedSlot != null) {
            if (!bookedEventCalendar.contains(selectedSlot)) {
                bookedEventCalendar.add(selectedSlot);
            }
            selectedSlot.incrementSlotCurrentCapacity();
        } else {
            throw new IllegalArgumentException("Incompatible time slots");
        }
    }


    public void cancelBooking(long startTime) {
        EventSlot selectedSlot = getSlotFromStartTime(startTime);
        selectedSlot.decrementSlotCurrentCapacity();
        if (selectedSlot.currentCapacity == 0) {
            bookedEventCalendar.remove(selectedSlot);
        }
    }

    public void rescheduleBooking(long oldStartTime, long newStartTime) {
        cancelBooking(oldStartTime);
        scheduleBooking(newStartTime);
    }

    private EventSlot getSlotFromStartTime(long startTime) {
        for (EventSlot eventSlot : bookedEventCalendar) {
            if (eventSlot.startTimestamp == startTime) {
                return eventSlot;
            }
        }

        throw new IllegalArgumentException("EventSlot not found or not booked");
    }

    private EventSlot getSlotForNewEvent(long startTimestamp) {

        for (EventSlot eventSlot : bookedEventCalendar) {
            //Make sure in eventWeekCalendar are saved only future events
            if (eventSlot.getStartTimestamp() == startTimestamp) {
                if (eventSlot.getCurrentCapacity() < eventCapacity) {
                    return eventSlot;
                } else {
                    return null;
                }
            }
        }
        return new EventSlot(startTimestamp, eventDuration);
    }

    public String writeNewEventOnDb(String shopId, final DatabaseCallback callback) {
        HashMap<String, Object> newEventHashMap = new HashMap<>();

        newEventHashMap.put("name", eventName);
        newEventHashMap.put("priceInCents", eventPriceInCents);
        newEventHashMap.put("duration", eventDuration);
        newEventHashMap.put("capacity", eventCapacity);

        final String[] newEventId = new String[1];

        db.collection("shops")
                .document(shopId)
                .collection("events")
                .add(newEventHashMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        callback.callback(documentReference);
                    }
                });

        return newEventId[0];
    }

    public void writeNewCalendarForEventOnDb(String shopId, String eventId) {
        for (Period period : eventWeekCalendar) {

            TimeOfWeek periodStart = period.getOpen();
            int startHour = periodStart.getTime().getHours();
            int startMinute = periodStart.getTime().getMinutes();
            int startDayOfWeek = periodStart.getDay().ordinal();

            TimeOfWeek periodEnd = period.getClose();
            int endHour = periodEnd.getTime().getHours();
            int endMinute = periodEnd.getTime().getMinutes();
            int endDayOfWeek = periodEnd.getDay().ordinal();

            HashMap<String, Object> eventWeekCalendarHashMap = new HashMap<>();

            eventWeekCalendarHashMap.put("startDay", startDayOfWeek);
            eventWeekCalendarHashMap.put("startTimeHour", startHour);
            eventWeekCalendarHashMap.put("startTimeMinute", startMinute);
            eventWeekCalendarHashMap.put("endDay", endDayOfWeek);
            eventWeekCalendarHashMap.put("endTimeHour", endHour);
            eventWeekCalendarHashMap.put("endTimeMinute", endMinute);

            db.collection("shops")
                    .document(shopId)
                    .collection("events")
                    .document(eventId)
                    .collection("periods")
                    .add(eventWeekCalendarHashMap);
        }
    }

    public void getShopEventByShopId(String id, final DatabaseCallback callback) {
        db.collection("shops")
                .document(id)
                .collection("events")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            callback.callback(task);
                        }
                    }
                });
    }

    private class EventSlot {
        private long startTimestamp;
        private long endTimestamp;
        private int currentCapacity;

        EventSlot(long startTimestamp, long duration) {
            this.startTimestamp = startTimestamp;
            this.endTimestamp = startTimestamp + duration;
            this.currentCapacity = 0;
        }

        long getStartTimestamp() {
            return startTimestamp;
        }

        public long getEndTimestamp() {
            return endTimestamp;
        }

        int getCurrentCapacity() {
            return currentCapacity;
        }

        void incrementSlotCurrentCapacity() {
            currentCapacity++;
        }

        void decrementSlotCurrentCapacity() {
            currentCapacity--;
        }
    }
}
