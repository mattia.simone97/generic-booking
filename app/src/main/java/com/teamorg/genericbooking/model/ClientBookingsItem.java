package com.teamorg.genericbooking.model;

import com.google.firebase.Timestamp;

import java.util.Calendar;

public class ClientBookingsItem {

    Calendar cal = Calendar.getInstance();

    private String eventName;
    private String shopName;
    private String eventStartStr;
    private long eventDurationMinutes;

    public ClientBookingsItem(String eventName, String shopName, Timestamp eventStartTimestamp, long eventDurationMinutes) {
        this.eventName = eventName;
        this.shopName = shopName;
        this.eventDurationMinutes = eventDurationMinutes;

        cal.setTime(eventStartTimestamp.toDate());
        this.eventStartStr = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
    }

    public String getEventName() {
        return eventName;
    }

    public String getShopName() {
        return shopName;
    }

    public String getEventStartStr() {
        return eventStartStr;
    }

    public long getEventDurationMinutes() {
        return eventDurationMinutes;
    }
}
