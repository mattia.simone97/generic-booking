package com.teamorg.genericbooking.model;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teamorg.genericbooking.DatabaseCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Shop {


    public Map<String, Object> shop;
    private String newShopId;

    private static FirebaseFirestore db = FirebaseFirestore.getInstance();

    public Shop() {
        this.shop = new HashMap<>();
    }

    public Shop(String userId, String shopName, String shopType, String shopEmail, String shopPhoneNumber, String shopAddress, double shopLat, double shopLng) {
        this.shop = new HashMap<>();

        shop.put("userId", userId);
        shop.put("name", shopName);
        shop.put("type", shopType);
        shop.put("email", shopEmail);
        shop.put("phoneNumber", shopPhoneNumber);
        shop.put("address", shopAddress);
        GeoPoint shopCoordinates = new GeoPoint(shopLat, shopLng);
        shop.put("addressCoordinates", shopCoordinates);
    }

    public String writeNewShops() {

        db.collection("shops")
                .add(shop)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        newShopId = documentReference.getId();
                        Log.d("Firestore: ", "DocumentSnapshot added with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                    }
                });

        return newShopId;
    }


    public void getShopByUserId(final String userId, final DatabaseCallback callback) {
        db.collection("shops")
                .whereEqualTo("userId", userId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            if (task.getResult().getDocuments().size() == 0) {
                                callback.callback(null);
                            }
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                callback.callback(document);
                            }
                        }
                    }
                });
    }

    public static void getShopById(String shopId, final DatabaseCallback callback) {
        FirebaseFirestore.getInstance().collection("shops")
                .document(shopId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        callback.callback(task.getResult().getData());
                    }
                });
    }

    public void setShopMap(Map<String, Object> data) {
        this.shop = data;
    }
}
