package com.teamorg.genericbooking.clientversion.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.teamorg.genericbooking.R;
import com.teamorg.genericbooking.model.ClientBookingsItem;

import java.util.List;

public class ClientBookingsAdapter extends RecyclerView.Adapter<ClientBookingsAdapter.ClientBookingsViewHolder> {

    List<ClientBookingsItem> clientBookingsItems;

    public ClientBookingsAdapter(List<ClientBookingsItem> clientBookingsItems) {
        this.clientBookingsItems = clientBookingsItems;
    }

    @NonNull
    @Override
    public ClientBookingsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_bookings_list_item, parent, false);

        return new ClientBookingsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClientBookingsViewHolder holder, int position) {
        ClientBookingsItem bookingsItem = clientBookingsItems.get(position);

        holder.eventNameTextView.setText("Appuntamento per " + bookingsItem.getEventName());
        holder.shopNameTextView.setText(bookingsItem.getShopName());
        holder.eventStartTextView.setText(bookingsItem.getEventStartStr());
        holder.eventDurationTextView.setText(bookingsItem.getEventDurationMinutes() + "minuti");

    }

    @Override
    public int getItemCount() {
        return clientBookingsItems.size();
    }

    public class ClientBookingsViewHolder extends RecyclerView.ViewHolder{

        TextView eventNameTextView;
        TextView shopNameTextView;
        TextView eventStartTextView;
        TextView eventDurationTextView;

        public ClientBookingsViewHolder(@NonNull View itemView) {
            super(itemView);

            eventNameTextView = itemView.findViewById(R.id.event_name_client_bookings);
            shopNameTextView = itemView.findViewById(R.id.shop_name_client_bookings);
            eventStartTextView = itemView.findViewById(R.id.start_hour_client_booking);
            eventDurationTextView = itemView.findViewById(R.id.event_duration_client_bookings);
        }
    }
}
