package com.teamorg.genericbooking.clientversion.ui.bookings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.Timestamp;
import com.teamorg.genericbooking.DatabaseCallback;
import com.teamorg.genericbooking.R;
import com.teamorg.genericbooking.clientversion.ClientMainActivity;
import com.teamorg.genericbooking.clientversion.adapters.ClientBookingsAdapter;
import com.teamorg.genericbooking.model.Booking;
import com.teamorg.genericbooking.model.ClientBookingsItem;
import com.teamorg.genericbooking.model.Shop;
import com.teamorg.genericbooking.model.ShopEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ClientBookingsFragment extends Fragment {

    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;

    List<ClientBookingsItem> bookingsItems;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_client_bookings, container, false);
        ClientMainActivity parentActivity = (ClientMainActivity) getActivity();

        recyclerView = root.findViewById(R.id.client_booking_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(parentActivity));

        showBookingsForUser(parentActivity.currentUser.getUid());

        return root;
    }

    private void showBookingsForUser(String userId) {

        Booking.getBookingsForUserId(userId, new DatabaseCallback() {
            @Override
            public void callback(Object result) {
                Map<String, Map<String, Object>> rawBookingsList = (Map<String, Map<String, Object>>) result;
                bookingsItems = new ArrayList<>();
                adapter = new ClientBookingsAdapter(bookingsItems);
                recyclerView.setAdapter(adapter);

                for (final Map.Entry<String, Map<String, Object>> rawBooking : rawBookingsList.entrySet()) {

                    final String[] eventName = new String[1];
                    final String[] shopName = new String[1];

                    String eventId = rawBooking.getValue().get("eventId").toString();
                    ShopEvent.getEventById(eventId, new DatabaseCallback() {
                        @Override
                        public void callback(Object result) {
                            eventName[0] = ((Map<String, Object>) result).get("name").toString();

                            String shopId = rawBooking.getValue().get("shopId").toString();
                            Shop.getShopById(shopId, new DatabaseCallback() {
                                @Override
                                public void callback(Object result) {
                                    shopName[0] = ((Map<String, Object>) result).get("name").toString();

                                    Timestamp startTimestamp = (Timestamp) rawBooking.getValue().get("startTimestamp");
                                    long durationInMinute = (long) rawBooking.getValue().get("duration");

                                    ClientBookingsItem clientBookingsItem = new ClientBookingsItem(
                                            eventName[0], shopName[0],
                                            startTimestamp, durationInMinute
                                    );

                                    bookingsItems.add(clientBookingsItem);
                                    adapter.notifyDataSetChanged();
                                }
                            });
                        }
                    });
                }
            }
        });
    }
}
