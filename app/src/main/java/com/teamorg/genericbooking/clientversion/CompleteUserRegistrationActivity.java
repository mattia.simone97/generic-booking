package com.teamorg.genericbooking.clientversion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.teamorg.genericbooking.DatabaseCallback;
import com.teamorg.genericbooking.model.GenericBookingUser;
import com.teamorg.genericbooking.R;
import com.teamorg.genericbooking.UserProfileActivity;

public class CompleteUserRegistrationActivity extends AppCompatActivity {

    FirebaseUser currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_user_registration);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        TextView emailTextView = findViewById(R.id.complete_registration_user_email);
        emailTextView.setText(currentUser.getEmail());

        final Button completeRegistrationBtn = findViewById(R.id.complete_registration_button);
        completeRegistrationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                completeRegistration();
            }
        });

    }

    public void completeRegistration() {
        EditText fullNameEditText = findViewById(R.id.complete_registraion_user_name_input);
        String fullName = fullNameEditText.getText().toString();

        EditText phoneNumberEditText = findViewById(R.id.complete_registraion_user_phone_input);
        String phoneNumber = phoneNumberEditText.getText().toString();

        GenericBookingUser clientUser = new GenericBookingUser();
        clientUser.setUserDetails(fullName, currentUser.getEmail(), phoneNumber);
        clientUser.writeNewUser(new DatabaseCallback() {
            @Override
            public void callback(Object result) {
                finishFirstLogin();
            }
        });


    }

    private void finishFirstLogin() {
        Intent userProfileIntent = new Intent(this, UserProfileActivity.class);

        startActivity(userProfileIntent);
    }
}
