package com.teamorg.genericbooking;

public class ListItem {

    private String shopName;
    private String shopType;
    private String shopDistance;

    public ListItem(String shopName, String shopType, String shopDistance) {
        this.shopName = shopName;
        this.shopType = shopType;
        this.shopDistance = shopDistance;
    }

    public String getShopName() {
        return shopName;
    }

    public String getShopType() {
        return shopType;
    }

    public String getShopDistance() {
        return shopDistance;
    }
}
