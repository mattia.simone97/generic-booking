package com.teamorg.genericbooking;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.libraries.places.api.model.Period;
import com.teamorg.genericbooking.model.ShopEvent;
import com.teamorg.genericbooking.shopmanager.ShopManagerMainActivity;

import java.util.ArrayList;

public class PeriodSetterShopEventActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout linearLayout;
    ViewPager viewPager;
    ArrayList<DailyOpeningHoursFragment> fragmentList = new ArrayList<>();
    ArrayList<Period> calendarPeriodsList = new ArrayList<>();

    String shopId;
    String eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_period_setter_shop_event);

        Intent intentFromPreviousStep = getIntent();
        shopId = intentFromPreviousStep.getStringExtra("shopId");
        eventId = intentFromPreviousStep.getStringExtra("eventId");

        Button mondayBtn = findViewById(R.id.button_monday);
        mondayBtn.setOnClickListener(this);
        Button tuesdayBtn = findViewById(R.id.button_tuesday);
        tuesdayBtn.setOnClickListener(this);
        Button wednesdayBtn = findViewById(R.id.button_wednesday);
        wednesdayBtn.setOnClickListener(this);
        Button thursdayBtn = findViewById(R.id.button_thursday);
        thursdayBtn.setOnClickListener(this);
        Button fridayBtn = findViewById(R.id.button_friday);
        fridayBtn.setOnClickListener(this);
        Button saturdayBtn = findViewById(R.id.button_saturday);
        saturdayBtn.setOnClickListener(this);
        Button sundayBtn = findViewById(R.id.button_sunday);
        sundayBtn.setOnClickListener(this);

        linearLayout = findViewById(R.id.day_week_selector);
        viewPager=findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(7);

        MyAdapter adapter = new MyAdapter(this, getSupportFragmentManager(), 7);
        viewPager.setAdapter(adapter);


    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    @Override
    public void onClick(@NonNull View v) {
        switch (v.getId()) {
            case R.id.button_monday:
                viewPager.setCurrentItem(0);
                break;
            case R.id.button_tuesday:
                viewPager.setCurrentItem(1);
                break;
            case R.id.button_wednesday:
                viewPager.setCurrentItem(2);
                break;
            case R.id.button_thursday:
                viewPager.setCurrentItem(3);
                break;
            case R.id.button_friday:
                viewPager.setCurrentItem(4);
                break;
            case R.id.button_saturday:
                viewPager.setCurrentItem(5);
                break;
            case R.id.button_sunday:
                viewPager.setCurrentItem(6);
                break;
        }

    }

    public void saveAndSetCalendar(@NonNull View v) {
        for (int i = 0; i < 7; i++) {
            getData(i);
        }

        ShopEvent shopEvent = new ShopEvent();
        shopEvent.setEventWeekCalendar(calendarPeriodsList);
        Log.i("Ids", shopId + " " + eventId);
        shopEvent.writeNewCalendarForEventOnDb(shopId, eventId);

        Intent terminateEventShopCreation = new Intent(this, ShopManagerMainActivity.class);
        startActivity(terminateEventShopCreation);
    }

    public void getData(int position) {
        DailyOpeningHoursFragment fragment = fragmentList.get(position);
        Period morning = fragment.getMorningPeriod(position);
        Period afternoon = fragment.getAfternoonPeriod(position);
        calendarPeriodsList.add(morning);
        calendarPeriodsList.add(afternoon);
    }

    private class MyAdapter extends FragmentPagerAdapter {
        private Context myContext;
        int totalTabs;

        MyAdapter(Context context, FragmentManager fm, int totalTabs) {
            super(fm);
            myContext = context;
            this.totalTabs = totalTabs;
        }


        @Override
        public Fragment getItem(int position) {
            //TODO: Fix bug on screen rotation
            DailyOpeningHoursFragment fragment = new DailyOpeningHoursFragment(position);
            /*Bundle args = new Bundle();
            args.putInt("pageNumber", position);
            fragment.setArguments(args);*/
            return fragment;
        }


        @Override
        public int getCount() {
            return 7;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            DailyOpeningHoursFragment createdFragment = (DailyOpeningHoursFragment) super.instantiateItem(container, position);
            fragmentList.add(position, createdFragment);
            return createdFragment;
        }
    }



}

