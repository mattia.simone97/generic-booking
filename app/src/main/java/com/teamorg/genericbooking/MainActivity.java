package com.teamorg.genericbooking;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.firestore.GeoPoint;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.concurrent.FutureTask;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    private List<ListItem> shopList;
    private Location currentLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        recyclerView = findViewById(R.id.shop_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        requestShopList();

    }

    private void showAllShops() {
        MainActivityDatabaseFilter allShopFilter = new MainActivityDatabaseFilter();
        allShopFilter.fetchAllShops(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If request is cancelled, the result arrays are empty.
        if (requestCode == 1) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
            }
        }
    }

    public void showShopList(List<Map<String, Object>> shopListFromDb) {
        shopList = new ArrayList<>();

        for (Map<String, Object> shopFromDb : shopListFromDb) {
            GeoPoint shopCoordinatesFromDb = (GeoPoint) shopFromDb.get("addressCoordinates");
            Location shopCoordinates = new Location("");
            shopCoordinates.setLatitude(shopCoordinatesFromDb.getLatitude());
            shopCoordinates.setLongitude(shopCoordinatesFromDb.getLongitude());

            float distanceInMeters = currentLocation.distanceTo(shopCoordinates);
            int distanceInMetersToShow = Math.round(distanceInMeters);
            ListItem listItem = new ListItem(
                    shopFromDb.get("name").toString(),
                    shopFromDb.get("type").toString(),
                    "Distanza " + distanceInMetersToShow + "m"
            );
            shopList.add(listItem);
        }

        adapter = new ShopListAdapter(shopList, this);
        recyclerView.setAdapter(adapter);
    }

    public void requestShopList() {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }

        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (location != null && location.getTime() > Calendar.getInstance().getTimeInMillis() - (10 * 60 * 1000)) {
            currentLocation = location;
            showAllShops();
        } else {

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 150, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    currentLocation = location;
                    showAllShops();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
    }

}
