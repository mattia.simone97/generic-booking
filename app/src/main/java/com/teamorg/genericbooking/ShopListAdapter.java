package com.teamorg.genericbooking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ShopViewHolder> {

    private List<ListItem> shopList;
    private Context context;

    public ShopListAdapter(List<ListItem> shopList, Context context) {
        this.shopList = shopList;
        this.context = context;
    }

    @Override
    public ShopListAdapter.ShopViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_list_item, parent, false);

        return new ShopViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShopViewHolder holder, int position) {
        ListItem shopItem = shopList.get(position);

        holder.textViewShopName.setText(shopItem.getShopName());
        holder.textViewShopType.setText(shopItem.getShopType());
        holder.textViewShopDistance.setText(shopItem.getShopDistance());
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }

    public class ShopViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewShopName;
        public TextView textViewShopType;
        public TextView textViewShopDistance;

        public ShopViewHolder(View itemView) {
            super(itemView);

            textViewShopName = itemView.findViewById(R.id.shop_name);
            textViewShopType = itemView.findViewById(R.id.shop_type);
            textViewShopDistance = itemView.findViewById(R.id.shop_distance);
        }
    }

}
