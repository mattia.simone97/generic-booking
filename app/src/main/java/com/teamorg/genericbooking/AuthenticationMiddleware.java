package com.teamorg.genericbooking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class AuthenticationMiddleware {

    private Context context;
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();


    public FirebaseUser user;

    public AuthenticationMiddleware(@NonNull Context context) {

        this.context = context;
        setCurrentUser();
    }

    public static void logout() {
    }

    private void setCurrentUser() {
        user = firebaseAuth.getCurrentUser();
    }

    public FirebaseUser isAuthenticatedOrLogin() {
        setCurrentUser();
        if (user == null) {
            startLogin();
            return null;
        } else {
            return user;
        }
    }

    private void startLogin() {
        Intent startLoginIntent = new Intent(context, LoginActivity.class);
        context.startActivity(startLoginIntent);

    }
}
