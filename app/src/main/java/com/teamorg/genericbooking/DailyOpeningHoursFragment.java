package com.teamorg.genericbooking;

import android.annotation.SuppressLint;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.libraries.places.api.model.DayOfWeek;
import com.google.android.libraries.places.api.model.LocalTime;
import com.google.android.libraries.places.api.model.Period;
import com.google.android.libraries.places.api.model.TimeOfWeek;

import java.util.Calendar;


public class DailyOpeningHoursFragment extends Fragment {

    private int num;
    LocalTime morningStartLocalTime;
    LocalTime morningEndLocalTime;
    LocalTime afternoonStartLocalTime;
    LocalTime afternoonEndLocalTime;


    EditText startHourMorningEditText;
    EditText endHourMorningEditText;
    EditText startHourAfternoonEditText;
    EditText endHourAfternoonEditText;

    public DailyOpeningHoursFragment(int num) {
        this.num = num;
        morningStartLocalTime = LocalTime.newInstance(0,0);
        morningEndLocalTime = LocalTime.newInstance(0,0);
        afternoonStartLocalTime = LocalTime.newInstance(0,0);
        afternoonEndLocalTime = LocalTime.newInstance(0,0);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.week_calendar_fragment_pager_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        /*Bundle args = getArguments();
        if (args != null) {
            num = args.getInt("pageNumber");
        } else {
            num = 1;
        }*/
        ((TextView) view.findViewById(R.id.weekday_text))
                .setText(getWeekDayName(num));

        startHourMorningEditText = view.findViewById(R.id.time_start_morning_input);
        endHourMorningEditText = view.findViewById(R.id.time_end_morning_input);
        startHourAfternoonEditText = view.findViewById(R.id.time_start_afternoon_input);
        endHourAfternoonEditText = view.findViewById(R.id.time_end_afternoon_input);

        Calendar currentTime = Calendar.getInstance();
        final int hour = currentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = currentTime.get(Calendar.MINUTE);
        View.OnClickListener openTimePickerClickListener = new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        switch (v.getId()) {
                            case R.id.time_start_morning_input:
                                startHourMorningEditText.setText(hourOfDay + ":" + minute);
                                morningStartLocalTime = LocalTime.newInstance(hourOfDay,minute);
                                break;
                            case R.id.time_end_morning_input:
                                endHourMorningEditText.setText(hourOfDay + ":" + minute);
                                morningEndLocalTime = LocalTime.newInstance(hourOfDay,minute);
                                break;
                            case R.id.time_start_afternoon_input:
                                startHourAfternoonEditText.setText(hourOfDay + ":" + minute);
                                afternoonStartLocalTime = LocalTime.newInstance(hourOfDay,minute);
                                break;
                            case R.id.time_end_afternoon_input:
                                endHourAfternoonEditText.setText(hourOfDay + ":" + minute);
                                afternoonEndLocalTime = LocalTime.newInstance(hourOfDay,minute);
                                break;
                        }

                    }
                }, hour, minute, true);
                timePickerDialog.setTitle("Seleziona l'orario");
                timePickerDialog.show();
            }
        };

        startHourMorningEditText.setOnClickListener(openTimePickerClickListener);
        endHourMorningEditText.setOnClickListener(openTimePickerClickListener);
        startHourAfternoonEditText.setOnClickListener(openTimePickerClickListener);
        endHourAfternoonEditText.setOnClickListener(openTimePickerClickListener);
    }

    private String getWeekDayName(int num) {
        String dayStr = null;
        switch (num) {
            case 0:
                dayStr = "Lunedì";
                break;
            case 1:
                dayStr = "Martedì";
                break;
            case 2:
                dayStr = "Mercoledì";
                break;
            case 3:
                dayStr = "Giovedì";
                break;
            case 4:
                dayStr = "Venerdì";
                break;
            case 5:
                dayStr = "Sabato";
                break;
            case 6:
                dayStr = "Domenica";
                break;
        }
        return dayStr;
    }

    private DayOfWeek getWeekDay(int num) {
        DayOfWeek day = null;
        switch (num) {
            case 0:
                day = DayOfWeek.MONDAY;
                break;
            case 1:
                day = DayOfWeek.TUESDAY;
                break;
            case 2:
                day = DayOfWeek.WEDNESDAY;
                break;
            case 3:
                day = DayOfWeek.THURSDAY;
                break;
            case 4:
                day = DayOfWeek.FRIDAY;
                break;
            case 5:
                day = DayOfWeek.SATURDAY;
                break;
            case 6:
                day = DayOfWeek.SUNDAY;
                break;
        }

        return day;
    }

    public Period getMorningPeriod(int position) {
        TimeOfWeek startTimeOfWeek = TimeOfWeek.newInstance(getWeekDay(position), morningStartLocalTime);
        TimeOfWeek endTimeOfWeek = TimeOfWeek.newInstance(getWeekDay(position), morningEndLocalTime);
        Period.Builder morningPeriodBuilder = Period.builder();
        morningPeriodBuilder.setOpen(startTimeOfWeek);
        morningPeriodBuilder.setClose(endTimeOfWeek);
        return morningPeriodBuilder.build();

    }

    public Period getAfternoonPeriod(int position) {
        TimeOfWeek startTimeOfWeek = TimeOfWeek.newInstance(getWeekDay(position), afternoonStartLocalTime);
        TimeOfWeek endTimeOfWeek = TimeOfWeek.newInstance(getWeekDay(position), afternoonEndLocalTime);
        Period.Builder morningPeriodBuilder = Period.builder();
        morningPeriodBuilder.setOpen(startTimeOfWeek);
        morningPeriodBuilder.setClose(endTimeOfWeek);
        return morningPeriodBuilder.build();
    }
}
