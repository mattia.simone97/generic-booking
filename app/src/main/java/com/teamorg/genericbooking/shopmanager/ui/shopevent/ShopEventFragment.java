package com.teamorg.genericbooking.shopmanager.ui.shopevent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.teamorg.genericbooking.DatabaseCallback;
import com.teamorg.genericbooking.R;
import com.teamorg.genericbooking.model.Shop;
import com.teamorg.genericbooking.model.ShopEvent;
import com.teamorg.genericbooking.shopmanager.ShopManagerMainActivity;

import java.util.ArrayList;
import java.util.List;

public class ShopEventFragment extends Fragment {

    ArrayAdapter<String> eventArrayAdapter;
    TextView shopNameTextView;
    List<String> eventList;
    ListView eventListView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.shop_event_fragment, container, false);

        final ShopManagerMainActivity parentActivity = (ShopManagerMainActivity) getActivity();
        shopNameTextView = root.findViewById(R.id.console_shop_name_title);
        setShopName(parentActivity.currentUser, new Shop());

        eventListView = root.findViewById(R.id.shop_event_list);

        FloatingActionButton createShopEventFab = root.findViewById(R.id.create_shop_event_fab);
        createShopEventFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.startCreateNewEventActivity();
            }
        });

        return root;
    }

    void setShopName(FirebaseUser user, final Shop shop) {
        shop.getShopByUserId(user.getUid(), new DatabaseCallback() {
            @Override
            public void callback(Object result) {
                if (result != null) {
                    QueryDocumentSnapshot resultDocumentSnapshot = (QueryDocumentSnapshot) result;
                    shopNameTextView.setText(resultDocumentSnapshot.get("name").toString());
                    getAndSetShopEventList(resultDocumentSnapshot.getId());

                }
            }
        });

    }

    void getAndSetShopEventList(String shopId) {
        ShopEvent shopEvent = new ShopEvent();
        shopEvent.getShopEventByShopId(shopId, new DatabaseCallback() {
            @Override
            public void callback(Object result) {
                eventList = new ArrayList<>();
                Task<QuerySnapshot> taskResults = (Task<QuerySnapshot>) result;
                for (DocumentSnapshot event : taskResults.getResult()) {
                    eventList.add(event.get("name").toString());
                }

                setEventArrayAdapter(eventList);
                eventListView.setAdapter(eventArrayAdapter);
            }
        });

    }


    void setEventArrayAdapter(List<String> eventListName) {
        eventArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, eventListName);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }
}
