package com.teamorg.genericbooking.shopmanager;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.teamorg.genericbooking.R;
import com.teamorg.genericbooking.model.Shop;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ShopRegistrationActivity extends AppCompatActivity {
    private static final String TAG = "Ricerca indirizzo";
    private static final String apiKey = "AIzaSyCDTEIA_rp9Ytb3rnfeY3oskEl66Cdpnxk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_registration);
        Places.initialize(getApplicationContext(), apiKey);

        Intent intentFromParent = getIntent();
        final String userId = intentFromParent.getStringExtra("userId");


        final Button registrationButton = findViewById(R.id.registration_button);
        registrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerShop(userId);
            }
        });
    }

    private void registerShop(String user) {
        EditText shopNameInput = findViewById(R.id.registration_input_shop_name);
        String shopName = shopNameInput.getText().toString();

        EditText shopTypeInput = findViewById(R.id.registration_input_shop_type);
        String shopType = shopTypeInput.getText().toString();

        EditText shopEmailInput = findViewById(R.id.registration_input_shop_email);
        String shopEmail = shopEmailInput.getText().toString();

        EditText shopPhoneNumberInput = findViewById(R.id.registration_input_shop_phone_number);
        String shopPhoneNumber = shopPhoneNumberInput.getText().toString();

        EditText shopAddressInput = findViewById(R.id.registration_input_shop_address);
        String shopAddress = shopAddressInput.getText().toString();

        Geocoder geocoder = new Geocoder(getApplicationContext());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocationName(shopAddress,1);
            double shopLat = addresses.get(0).getLatitude();
            double shopLng = addresses.get(0).getLongitude();
            Log.i("coord: ", shopLat + ", " + shopLng);
            Shop newShop = new Shop(user, shopName, shopType, shopEmail, shopPhoneNumber, shopAddress, shopLat, shopLng);
            String newShopId = newShop.writeNewShops();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Toast.makeText(this,"Attvità registrata", Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, ShopManagerMainActivity.class);
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                EditText registrationAddressEditText = findViewById(R.id.registration_input_shop_address);
                registrationAddressEditText.setText(place.getAddress());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            }
        }
    }

    public void searchAddress(View v) {
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS);

        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.OVERLAY,fields
        ).build(this);

        startActivityForResult(intent,1);

    }



}
