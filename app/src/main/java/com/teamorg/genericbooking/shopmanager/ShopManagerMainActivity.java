package com.teamorg.genericbooking.shopmanager;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.teamorg.genericbooking.AuthenticationMiddleware;
import com.teamorg.genericbooking.CreateShopEventActivity;
import com.teamorg.genericbooking.DatabaseCallback;
import com.teamorg.genericbooking.R;
import com.teamorg.genericbooking.clientversion.ClientMainActivity;
import com.teamorg.genericbooking.model.Shop;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import java.util.List;

public class ShopManagerMainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;

    public FirebaseUser currentUser;
    Shop currentShop;
    List<String> eventListName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_navigation);

        currentUser = getAuthUser(new AuthenticationMiddleware(this));
        checkIfUserHasAshopOrStartCreateShop(currentUser, new Shop());

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        drawer = findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        );
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_shop_events,
                R.id.nav_dashboard,
                R.id.nav_shop_details,
                R.id.nav_settings)
                .setDrawerLayout(drawer)
                .build();

        NavigationView navigationView = findViewById(R.id.nav_view);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        TextView switchToShopModeBtn = findViewById(R.id.pass_to_client_mode_button);
        switchToShopModeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent switchToShopModeIntent = new Intent(getBaseContext(), ClientMainActivity.class);
                finish();
                startActivity(switchToShopModeIntent);
            }
        });

        TextView logoutBtn = findViewById(R.id.logout_shop_button);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthenticationMiddleware.logout();
            }
        });
    }

    @NonNull
    public void checkIfUserHasAshopOrStartCreateShop(final FirebaseUser user, final Shop shop) {
        shop.getShopByUserId(currentUser.getUid(), new DatabaseCallback() {
            @Override
            public void callback(Object result) {
                if (result == null) {
                    createShopForUser(user);
                }
            }
        });

    }

    private FirebaseUser getAuthUser(AuthenticationMiddleware authenticationMiddleware) {
        return authenticationMiddleware.isAuthenticatedOrLogin();
    }

    void createShopForUser(FirebaseUser user) {
        Intent createNewShopIntent = new Intent(this, ShopRegistrationActivity.class);
        createNewShopIntent.putExtra("userId", user.getUid());
        startActivity(createNewShopIntent);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public void startCreateNewEventActivity() {
        Intent newEventIntent = new Intent(this, CreateShopEventActivity.class);
        startActivity(newEventIntent);
    }
}
