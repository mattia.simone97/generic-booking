package com.teamorg.genericbooking;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.teamorg.genericbooking.model.Shop;
import com.teamorg.genericbooking.model.ShopEvent;

public class CreateShopEventActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_shop_event);
    }

    public void nextStepCreateShopEvent(View v) {

        EditText eventNameEditText = findViewById(R.id.create_shop_event_name_input);
        String eventName = eventNameEditText.getText().toString();

        EditText eventPriceEditText = findViewById(R.id.create_shop_event_price_input);
        double eventPrice = Double.valueOf(eventPriceEditText.getText().toString());
        int eventPriceInCents = (int) (eventPrice * 100);

        EditText eventDurationEditText = findViewById(R.id.create_shop_event_duration_input);
        int eventDuration = Integer.valueOf(eventDurationEditText.getText().toString());

        int eventCapacity = 1;

        final ShopEvent newShopEvent = new ShopEvent(eventName, eventPriceInCents, eventDuration, eventCapacity);
        final Context context = this;
        Shop shop = new Shop();
        AuthenticationMiddleware am = new AuthenticationMiddleware(getApplicationContext());
        shop.getShopByUserId(am.user.getUid(), new DatabaseCallback() {
            @Override
            public void callback(Object result) {
                final Intent intent = new Intent(context, PeriodSetterShopEventActivity.class);
                final DocumentSnapshot shopRes = (DocumentSnapshot) result;
                newShopEvent.writeNewEventOnDb(shopRes.getId(), new DatabaseCallback() {
                    @Override
                    public void callback(Object result) {
                        DocumentReference resDocumentReference = (DocumentReference) result;
                        intent.putExtra("shopId", shopRes.getId());
                        intent.putExtra("eventId", resDocumentReference.getId());
                        startActivity(intent);
                    }
                });

            }
        });

    }
}
