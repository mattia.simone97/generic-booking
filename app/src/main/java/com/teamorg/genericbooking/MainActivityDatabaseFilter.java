package com.teamorg.genericbooking;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivityDatabaseFilter {

    private final static String TAG = "Shop query ";
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    private List<Map<String, Object>> results = new ArrayList<>();

    public FirebaseFirestore getDb() {
        return db;
    }

    public MainActivityDatabaseFilter() {

    }

    public void fetchAllShops(final MainActivity mainActivity) {

        CollectionReference allShopsRef = db.collection("shops");

        allShopsRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        results.add(document.getData());
                    }
                    mainActivity.showShopList(results);

                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }
}
