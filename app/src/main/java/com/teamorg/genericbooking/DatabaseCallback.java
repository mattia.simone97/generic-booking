package com.teamorg.genericbooking;

public interface DatabaseCallback {
    void callback(Object result);
}
